package com.app.Repository;

import org.springframework.data.repository.CrudRepository;
import com.app.Domain.Student;

public interface StudentRepository extends CrudRepository<Student, Integer>
{
}

