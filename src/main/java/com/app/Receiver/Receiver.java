package com.app.Receiver;

import com.app.Domain.Student;
import com.app.Sender.Sender;
import com.app.Service.StudentService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.Integer.parseInt;

@RabbitListener(queues = "c2r")
public class Receiver {

    //autowired the StudentService class
    @Autowired
    StudentService studentService;

    @Autowired
    Student student;

    @Autowired
    Sender sender;

    @RabbitHandler
    public void receive(String message) {
        String[] buffer = message.split("-");

        student.setId(parseInt(buffer[0]));
        student.setAge(parseInt(buffer[1]));
        student.setName(buffer[2]);
        student.setEmail(buffer[3]);

        studentService.saveOrUpdate(student);
        System.out.println(" [x] Received '" +
                student.getId()+"-"+
                student.getAge()+"-"+
                student.getName()+"-"+
                student.getEmail()+"'");

        sender.send("created field with {" +
                "id: " + student.getId()+
                ", age: " + student.getAge()+
                ", name: " + student.getName()+
                ", email: " + student.getEmail());
    }
}
